package attestation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public class Main {

    public static final String FIRST = "C:\\Users\\User\\Desktop\\мое";

    public static void main(String[] args) {
        try (var stream = Files.lines(Paths.get(FIRST))) {
            List<User> users = stream
                    .map(Utils::parseUser)
                    .toList();
            System.out.println(users);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}



