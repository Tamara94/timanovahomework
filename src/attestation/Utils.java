package attestation;

public class Utils
{
    public static final String FILE = "src/attestation/user";
    private Utils(){
        throw new UnsupportedOperationException("Utility classes should not be instantiated");
    }
    public static User parseUser(String line) {
        String[] split = line.split("\\|");
        return new User(Integer.parseInt(split[0]),
                split[1],
                split[2],
                Integer.parseInt(split[3]),
                Boolean.parseBoolean(split[4]));

    }
}

