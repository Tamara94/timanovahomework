package attestation;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class User {
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", employed=" + employed +
                '}';
    }

    private List<User> listAllUsers()
    {
        try(Stream<Path> files = Files.list(Paths.get("C:\\Users\\User\\Desktop\\мое")))
        {
            return files.map((Path line) -> {
                return Utils.parseUser(String.valueOf(line));
            }).toList();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return List.of();
        }
    }

    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private boolean employed;

    public User(int id, String firstName, String lastName, int age, boolean employed) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.employed = employed;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isEmployed() {
        return employed;
    }

    public void setEmployed(boolean employed) {
        this.employed = employed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && age == user.age && employed == user.employed && firstName.equals(user.firstName) && lastName.equals(user.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, age, employed);
    }
}

